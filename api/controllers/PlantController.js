/**
 * PlantController
 *
 * @description :: Server-side logic for managing plants
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */


module.exports = {
	create: function (req, res) {
			if(req.method === 'GET')
			return res.json({'status':'GET not allowed'});						
		//	Call to /upload via GET is error
		var uploadFile = req.file('image');
		//Use this to upload to custom folder
		//If you don't want this remove {dirname: ''}
		//There are other options also .Check at skipper docs
//gggggggg
		//If dirname is not set the upload will be done to ./tmp/uploads
		
		
	    uploadFile.upload({ adapter: require('skipper-gridfs'),
},function onUploadComplete (err, files) {				
	    // Files will be uploaded to /assets/images/
	    // Access the files via localhost:1337/images/yourfilename
	    console.log(files);
	    
	    	function getFormData () {
	    		if (err) return res.serverError(err);								
	    	
	    	//	IF ERROR Return and send 500 error with error
	    	var foo=req.params.all()
	    	console.log(foo);
	    	var data={};
	    	data.image=files[0].fd;
	    	data.imageType=files[0].type;
	    	data.localName=req.param('localName');
	    	data.kingdom=req.param('kingdom');
	    	data.phylum=req.param('phylum');
	    	data.plantClass=req.param('plantClass');
	    	data.order=req.param('order');
	    	data.family=req.param('family');
	    	data.genus=req.param('genus');
	    	data.species=req.param('species');
	    	data.scientificName=req.param('scientificName');
	    	data.ethnomedical=req.param('ethnomedical');
	    	data.phytochemicals=req.param('phytochemicals');
	    	data.references=req.param('references');
	    	data.pharmachologicalActivity=req.param('pharmachologicalActivity');
	    	
	    	return data
	    	}
	    	function save(){
	    	
	    		Plant.create(getFormData(),function plantCreated(err, plant){
				if(err) return res.serverError(err);
				if(!plant) res.serverError(plant);

				
	    	console.log(plant);	

				plant.save(function(err,plant){
				if(err) return next(err);
			});
				res.redirect('/');
			
			});
	    	}
	    	save(getFormData());
	    	
	    
	    	
	    
	    	
	    	

	    	


			
			
	    	
	    	
	    });
	    
  },

  getFile:function(req,res){
  	 var blobAdapter = require('skipper-gridfs')({
    });

    var fd = req.param('fd'); // value of fd comes here from get request
    blobAdapter.read(fd, function(error , file) {
        if(error) {
            res.json(error);
        } else {
        	Plant.findOneByImage(fd,function imageFound(err,image){
        		res.contentType(image.imageType);
            	res.send(new Buffer(file));
        	});
            
        }
    });
  }
	
};

