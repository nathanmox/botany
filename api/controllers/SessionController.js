/**
 * SessionController
 *
 * @description :: Server-side logic for managing sessions
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	
	create:function  (req,res,next) {
		User.findOneByEmail(req.param('username'),function foundUserByEmail (err,user) {
			if(err) return next(err);
			if(!user){
				User.findOneByUsername(req.param('username'),function foundUserByUsername (err,user) {
					if(err) return next(err);
					if(!user){
						req.session.authenticated=false;
						res.locals.flash={err:'no user', message:'no such user'};
						 res.redirect('/login');
						 return;

					}
					if(user.password==req.param('password')){
						req.session.authenticated=true;
						res.redirect('/admin');
					}
					else{
						req.session.authenticated=false;
						res.locals.flash={err:'wrong credentials',message:' username/email or password missmatch'};
						res.redirect('/login');
					}
				})
			}
			else{
				if(user.password==req.param('password')){
						req.session.authenticated=true;
						res.redirect('/admin');
					}
					else{
						req.session.authenticated=false;
						res.locals.flash={err:'wrong credentials',message:' username/email or password missmatch'};
						res.redirect('/login');
					}

			}
		})
	},
	destroy:function(req,res,next){
		req.session.authenticated=false;
		res.redirect('/');
	}
	
};

