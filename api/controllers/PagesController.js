/**
 * PagesController
 *
 * @description :: Server-side logic for managing pages
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	


  /**
   * `PagesController.home()`
   */
  	home: function (req, res,next) {
  		res.view();
	},
	
	admin:function(req,res,next) {
		res.view();

	},

	login:function  (req,res,next) {
		res.view();
		res.locals.flash={};
	},
	single:function(req,res,next){
		console.log(req.param('id'));
		Plant.findOne(req.param('id'),function foundPlant(err, plant){
			if(err) return next(err);
			if(!plant) return next();
			console.log(plant);
		res.view({
			plant:plant
		});
	});
	}

};

