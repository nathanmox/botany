/**
* Plant.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
  	image:{
  		type:'string'
  	},
    imageType:{
      type:'string'
    },
    localName:{
      type:'string'
    },
    ethnomedical:{
      type:'string'
    },
  		kingdom:{
  		type:'string'
  	},
  	phylum:{
  		type:'string'

  	},
  	plantClass:{
  		type:'string'
  	},
  	order:{
  		type:'string'
  	},
    family:{
      type:'string'
    },
    genus:{
      type:'string'
    },
    species:{
      type:'string'
    },
    scientificName:{
      type:'string'
    },
    phytochemicals:{
      type:'string'
    },
    pharmachologicalActivity:{
      type:'string'
    },
    references:{
      type:'string'
    },
  	toJSON: function  () {
  		var obj=this.toObject();
  		delete obj.csrf;
  		return obj;
  		// body...
  	},

  }
};

