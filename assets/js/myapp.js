var myApp=angular.module('myApp',['ngResource']);

var searching=true;

myApp.run(function($rootScope) {
	$rootScope.address='http://localhost:1337/';
  $rootScope.dynamicSort = function (property) {
    var sortOrder = 1;
    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a,b) {
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    	}
	}

});



//factories
myApp.factory('Plant',['$resource',function($resource) {
return $resource('http://localhost:1337/'+'plant/:id',{id:'@id'},{update: { method: 'PUT' }});
}]);

//services

//filters
myApp.filter('searchFor', function(){

	// All filters must return a function. The first parameter
	// is the data that is to be filtered, and the second is an
	// argument that may be passed with a colon (searchFor:searchString)

	

	return function(arr, searchString){

		if(!searchString){
			return arr;
		}

		var result = [];

		searchString = searchString.toLowerCase();

		// Using the forEach helper method to loop through the array
		angular.forEach(arr, function(plant){

			if(plant.localName.toLowerCase().indexOf(searchString) !== -1){
				result.push(plant);
			}

		});

		return result;
	};

});


 myApp.controller('homeCtrl',['$scope','Plant', function($scope,Plant) {
 	$scope.address='http://localhost:1337/';
 	$scope.grid=true;
 	$scope.togleView=function(view){
 		if(view=='grid')
 		{
 			$scope.grid=true;
 		}
 		else{
 			$scope.grid=false;
 		}
 	}
 	$scope.view='list'
 	var plants=Plant.query({limit:6},function(){

			$scope.plants=plants;
			$scope.recentPlants=plants
			$scope.recentPlants.sort($scope.dynamicSort('-createdAt'));
			console.log(plants);

		});
 	$scope.delete = function(id,index) {
  	$scope.currentPlant=new Plant();
  	//set the id of the new enrollment instance to that passed from the view
  	$scope.currentPlant.id=id;
  	//send delete request to api
  	$scope.currentPlant.$delete();
  	//remove from scope hence UI.
  	$scope.plants.splice(index, 1);
  }
 	


 }]);
